const BaseController = require('../shared/BaseController');
const { httpResponse } = require('../shared/helpers/response');
const SalesTrackActivitiesUseCase = require('./useCase/SalesTrackActivitesUseCase');

class SalesTrackActivitiesController extends BaseController {
    constructor(salesTrackActivitiesUseCase) {
        super();
        this.salesTrackActivitiesUseCase = salesTrackActivitiesUseCase || new SalesTrackActivitiesUseCase();
    }

    async getList(req, res, next) {
        httpResponse(await this.salesTrackActivitiesUseCase.get(req.params, req.query), res);
    }
}

module.exports = SalesTrackActivitiesController;

const dayjs = require('dayjs');
const BaseUseCase = require('../../shared/BaseUseCase');
const GenericResponseEntity = require('../../shared/entities/GenericResponseEntity');

class SalesTrackActivitiesUseCase extends BaseUseCase {
    constructor() {
        super();
    }

    async get() {
        const response = new GenericResponseEntity();

        response.success = true;
        response.message = 'SUCCESS GET';
        response.data = 'f';

        return response;
    }
}

module.exports = SalesTrackActivitiesUseCase;

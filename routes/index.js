const express = require('express');
// const LoginCrmMiddleware = require('../modules/shared/middleware/LoginCrmMiddleware');
const SalesTrackActivitiesController = require('../modules/controllers/SalesTrackActivitiesController');
const router = express.Router();

router.get(
    '/getList',
    // LoginCrmMiddleware,
    async (req, res, next) => {
        try {
            await new SalesTrackActivitiesController().getList(req, res, next);
        } catch (e) {
            next(e);
        }
    }
);

module.exports = router;
